package com.likeandupdate;

import android.app.Activity;
import android.app.Application;
import android.widget.RelativeLayout;

import com.appfeature.AppsFeature;

public class AppApplication extends Application {

    private static AppApplication appApplication;


    public static AppApplication getInstance() {
        return appApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appApplication = this;
    }

    private AppsFeature appsFeature;

    public void initAppFeature(Activity activity) {
        if (appsFeature == null) {
            appsFeature = AppsFeature.getInstance(activity, BuildConfig.VERSION_CODE)
                    .setShowInLayoutRatingCard(true)
                    .setShowRatingDialog(true);

            appsFeature.init();
        }
    }

    public void showRating(RelativeLayout layout){
        if(appsFeature!=null){
            appsFeature.showRating(layout);
        }
    }
}
