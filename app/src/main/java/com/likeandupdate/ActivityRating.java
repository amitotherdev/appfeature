package com.likeandupdate;

import android.os.Bundle;
import android.widget.RelativeLayout;

import com.appfeature.AppsFeature;
import com.appfeature.interfaces.RemoteCallback;
import com.appfeature.utility.RemoteModel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ActivityRating extends AppCompatActivity {

    private AppsFeature appFeature;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        setRating();
    }
    private void setRating() {
        final RelativeLayout container = findViewById(R.id.rating_container);
        AppApplication.getInstance().showRating(container);
    }
}
