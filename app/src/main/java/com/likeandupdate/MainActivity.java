package com.likeandupdate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.likeandupdate.base.AppFeatureBaseActivity;


public class MainActivity extends AppFeatureBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    protected void onVersionUpdate() {

    }


    public void onRatingClick(View view) {
        startActivity(new Intent(MainActivity.this, ActivityRating.class));
    }

}
