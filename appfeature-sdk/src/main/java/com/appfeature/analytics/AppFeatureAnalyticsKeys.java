package com.appfeature.analytics;

public interface AppFeatureAnalyticsKeys {

    String IN_APP_REVIEW_API = "in_app_review_api";
    String IS_REVIEW_DIALOG_LAUNCH = "is_review_dialog_launch";
}
