package com.appfeature.analytics;

import android.content.Context;
import android.os.Bundle;

public class AppFeatureAnalytics extends AppFeatureAnalyticsUtil {

    private static volatile AppFeatureAnalytics sSoleInstance;

    private AppFeatureAnalytics(Context context) {
        super(context);
    }

    public static AppFeatureAnalytics getInstance(Context context) {
        if (sSoleInstance == null) {
            synchronized (AppFeatureAnalytics.class) {
                if (sSoleInstance == null) sSoleInstance = new AppFeatureAnalytics(context);
            }
        }
        return sSoleInstance;
    }

//    public void onLaunchInAppReview(String value) {
//        setUserProperty(AppFeatureAnalyticsKeys.IN_APP_REVIEW_API, value);
//    }

    //Log Events
    public void onLaunchInAppReview(String isReviewApiLaunch) {
        Bundle params = new Bundle();
        if (isReviewApiLaunch != null) {
            params.putString(AppFeatureAnalyticsKeys.IS_REVIEW_DIALOG_LAUNCH, isReviewApiLaunch);
        }
        sendEvent(AppFeatureAnalyticsKeys.IN_APP_REVIEW_API, params);
    }

}
