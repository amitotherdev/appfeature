package com.appfeature.feature;

import com.appfeature.interfaces.VersionCallback;
import com.appfeature.utility.Utility;
import com.appfeature.utility.Version;

/**
 * This class is used to display version update dialog on activity
 *
 * @author Abhijit Rao
 * @since 2018.12.07
 * @version 1.0
 */
public class VersionUpdate {

    private final Version version;
    private int appVersionCode;
    private VersionCallback versionCallback;

    private VersionUpdate(int appVersionCode, Version version) {
        this.version = version;
        this.appVersionCode = appVersionCode;
    }

    public static VersionUpdate newInstance(int appVersionCode, Version version) {
        return new VersionUpdate(appVersionCode, version);
    }

    public VersionUpdate setNotificationType(String notificationType) {
        Utility.log(notificationType);
        return this;
    }

    public VersionUpdate setListener(VersionCallback versionCallback) {
        this.versionCallback = versionCallback;
        return this;
    }

    public void init() {
        int mAppVersion = appVersionCode;
        int mMinVersion = version.getMinimumVersion();
        int mLatestVersion = version.getLatestVersionCode();
        if(mLatestVersion>mAppVersion){
            boolean restrictToUpdate;
            restrictToUpdate = mAppVersion < mMinVersion;
            if(versionCallback!=null){
                versionCallback.showVersionDialog(restrictToUpdate, version.getUiModel());
            }
        }
    }

}
