package com.appfeature;

import android.app.Activity;
import android.widget.RelativeLayout;

import com.appfeature.feature.RateUs;
import com.appfeature.feature.VersionUpdate;
import com.appfeature.interfaces.RemoteCallback;
import com.appfeature.interfaces.VersionCallback;
import com.appfeature.ui.VersionDialog;
import com.appfeature.utility.Rating;
import com.appfeature.utility.RemoteConfig;
import com.appfeature.utility.RemoteModel;
import com.appfeature.utility.UIModel;
import com.appfeature.utility.UserPreference;
import com.appfeature.utility.Utility;
import com.appfeature.utility.Version;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Abhijit Rao
 * @version 1.0
 * @since 2018.12.07
 */
public class AppsFeature {

    private static AppsFeature _instance;
    private final Activity activity;
    private int appVersionCode;
    private String mRemoteConfigStatus;
    private RemoteModel mRemoteData;
    private UserPreference userPref;
    private boolean isRemoteSync = false;
    //    private List<RemoteCallback> remoteCallbackList = new ArrayList<>();
    private List<VersionCallback> versionCallback = new ArrayList<>();
    private HashMap<Integer , RemoteCallback> remoteCallbackList = new HashMap<>();

    private boolean isShowInLayoutRatingCard = true;
    private boolean isShowRatingDialog = true;

    public boolean isRemoteSync() {
        return isRemoteSync;
    }

    private AppsFeature(Activity activity, int appVersionCode) {
        this.activity = activity;
        this.userPref = new UserPreference(activity, activity.getApplicationContext().getPackageName());
        this.appVersionCode = appVersionCode;
    }

    public static AppsFeature getInstance(Activity activity, int appVersionCode) {
        if (_instance == null) {
            _instance = new AppsFeature(activity, appVersionCode);
        }
        return _instance;
    }

//    public AppsFeature addRemoteCallback(RemoteCallback remoteCallback) {
//        if (remoteCallbackList != null && remoteCallback != null) {
//            remoteCallbackList.add(remoteCallback);
//        }
//        return this;
//    }

    public AppsFeature addVersionCallback(VersionCallback versionCallback) {
        this.versionCallback.add(versionCallback);
        return this;
    }

    public AppsFeature addRemoteCallback(int hashCode ,  RemoteCallback remoteCallback) {
        try {
            this.remoteCallbackList.put(hashCode , remoteCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public void removeRemoteCallback(int hashCode) {
        try {
            if ( remoteCallbackList != null && remoteCallbackList.size() > 0 && remoteCallbackList.get(hashCode) != null ){
                remoteCallbackList.remove(hashCode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AppsFeature init() {
        RemoteConfig.newInstance(activity)
                .initialize()
                .fetch(new RemoteCallback() {
                    @Override
                    public void onComplete(String status, RemoteModel remoteData) {
                        Utility.log(status);
                        isRemoteSync = true;
                        mRemoteConfigStatus = status;
                        mRemoteData = remoteData;
                        initRatingProcess();
                        if (versionCallback != null) {
                            initVersionProcess();
                        }
//                        updateCallback(status, remoteData);
                        refreshRemoteCallback();
                    }

                    @Override
                    public void onError(String message) {
                        Utility.log(message);
                    }

                });
        return this;
    }

    private void refreshRemoteCallback(){
        try {
            if ( remoteCallbackList != null && remoteCallbackList.size() > 0 ){
                for ( Integer integer : remoteCallbackList.keySet() ){
                    if (  remoteCallbackList.get(integer) != null ){
                        remoteCallbackList.get(integer).onComplete("true" , null);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void updateCallback(String status, RemoteModel remoteData) {
//        if (remoteCallbackList != null && remoteCallbackList.size() > 0) {
//            for (RemoteCallback remoteCallback : remoteCallbackList) {
//                if (remoteCallback != null) {
//                    remoteCallback.onComplete(null, null);
//                }
//            }
//        }
//    }

    public void initVersionProcess() {
        if (mRemoteData == null) {
            Utility.toast(activity, "Error :102 (Invalid Request)");
            return;
        }
        Version version = mRemoteData.getVersion();
        VersionUpdate.newInstance(appVersionCode, version)
                .setNotificationType(version.getNotificationType())
                .setListener(new VersionCallback() {
                    @Override
                    public void showVersionDialog(Boolean restrictToUpdate, UIModel uiData) {
                        VersionDialog.newInstance(activity, restrictToUpdate, uiData)
                                .show();
                        if (versionCallback != null) {
                            for (VersionCallback callback : versionCallback) {
                                callback.showVersionDialog(restrictToUpdate, uiData);
                            }
                        }
                    }
                })
                .init();
    }

    public void initRatingProcess() {
        if (mRemoteData == null) {
            Utility.toast(activity, "Error :103 (Invalid Request)");
            return;
        }
        Rating rating = mRemoteData.getRating();
        if (validateVisibility()) {
            RateUs.newInstance(activity)
                    .setSessionFirstInstallTime(rating.getSessionFirstInstallTime())
                    .setSessionRepeatTime(rating.getSessionRepeatTime())
                    .setSessionDifferenceTime(rating.getSessionDifferenceTime())
                    .setRatingShowOnlyOnce(rating.isRatingShowOnlyOnce())
                    .setRatingApi(rating.isRatingApi())
                    .setNegativeButtonPressedAndNotShowRating(rating.isNegativeButtonPressedAndNotShowRating())
                    .setUiData(rating.getUiModel())
                    .init();
        }
    }

    /**
     * @param relativeLayout : adding rating card view on this ViewGroup
     */
    public void showRating(RelativeLayout relativeLayout) {
        if (isShowInLayoutRatingCard && RateUs.getInstance() != null && relativeLayout != null) {
            RateUs.getInstance().showUI(relativeLayout);
        }
    }

    /**
     * Show Rating by In-App Review API
     */
   public void showRatingDialog() {
        if (isShowRatingDialog && RateUs.getInstance() != null) {
            RateUs.getInstance().showUI(null);
        }
    }

    private boolean validateVisibility() {
//        boolean isOnce = userPref.isRatingShowOnlyOnce();
//        boolean neverShow = userPref.isRatingNegativeButtonPressedAndNotShow();
        if (userPref.isRatingSubmitted()) {
            return false;
        }
        return true;
    }

    public boolean isShowInLayoutRatingCard() {
        return isShowInLayoutRatingCard;
    }

    public AppsFeature setShowInLayoutRatingCard(boolean showInLayoutRatingCard) {
        isShowInLayoutRatingCard = showInLayoutRatingCard;
        return this;
    }

    public boolean isShowRatingDialog() {
        return isShowRatingDialog;
    }

    public AppsFeature setShowRatingDialog(boolean showRatingDialog) {
        isShowRatingDialog = showRatingDialog;
        return this;
    }
}
