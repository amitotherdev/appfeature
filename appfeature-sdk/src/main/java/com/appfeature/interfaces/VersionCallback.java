package com.appfeature.interfaces;

import com.appfeature.utility.UIModel;

public interface VersionCallback {

    void showVersionDialog(Boolean restrictToUpdate, UIModel uiData);
}
