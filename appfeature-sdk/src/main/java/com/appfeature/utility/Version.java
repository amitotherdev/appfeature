package com.appfeature.utility;

public class Version {

    private int minimumVersion;
    private int latestVersionCode;
    private String notificationType;
    private UIModel uiModel;


    public UIModel getUiModel() {
        return uiModel;
    }

    public void setUiModel(UIModel uiModel) {
        this.uiModel = uiModel;
    }

    public int getMinimumVersion() {
        return minimumVersion;
    }

    public void setMinimumVersion(int minimumVersion) {
        this.minimumVersion = minimumVersion;
    }

    public int getLatestVersionCode() {
        return latestVersionCode;
    }

    public void setLatestVersionCode(int latestVersionCode) {
        this.latestVersionCode = latestVersionCode;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }
}
