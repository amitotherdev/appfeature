package com.appfeature.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class UserPreference {
    private static final String KEY_INSTALLATION_DATE = "init_date";
    private static final String KEY_LAST_USAGE_DATE = "last_date";
    private static final String KEY_RATING_SUBMITTED = "rate_done";
    private static final String KEY_FIRST_INSTALL_CHECK = "first_install_check";
    private static final String KEY_NEGATIVE_BUTTON_PRESSED = "negative_button_pressed";
    private static final String KEY_RATING_SHOW_ONCE = "rating_show_once";
    private final Context context;
    private final String packageName;
    private final SharedPreferences sharedPreferences;

    public UserPreference(Context context, String packageName) {
        this.context = context;
        this.packageName = packageName.replaceAll("\\.", "");
        sharedPreferences = context.getSharedPreferences( packageName , Context.MODE_PRIVATE);
    }

    private String getKey(String key) {
        return key ;
//        return packageName + key;
    }

    private void savePrefString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (context != null && value != null) {
            editor.putString(getKey(key), value);
            editor.apply();
        }
    }

    private String loadPrefString(String key) {
        if (context != null) {
            return sharedPreferences.getString(getKey(key), "");
        }
        return "";
    }

    private void savePrefBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (context != null) {
            editor.putBoolean(getKey(key), value);
            editor.apply();
        }
    }

    private Boolean loadPrefBoolean(String key) {
        return loadPrefBoolean(key, false);
    }

    private Boolean loadPrefBoolean(String key, boolean defaltValue) {
        if (context != null) {
            return sharedPreferences.getBoolean(getKey(key), defaltValue);
        }
        return false;
    }

    public String getInstallationDate() {
        String installationDate = loadPrefString(KEY_INSTALLATION_DATE);
        if (TextUtils.isEmpty(installationDate)) {
            installationDate = DateUtil.getDateStamp();
            savePrefString(KEY_INSTALLATION_DATE, installationDate);
        }
        return installationDate;
    }

    public void reInitializeInstallationDate() {
        savePrefString(KEY_INSTALLATION_DATE, DateUtil.getDateStamp());
    }

    public String getLastUsageDate() {
        String lastUsageDate = loadPrefString(KEY_LAST_USAGE_DATE);
        if (TextUtils.isEmpty(lastUsageDate)) {
            lastUsageDate = DateUtil.getDateStamp();
            savePrefString(KEY_LAST_USAGE_DATE, lastUsageDate);
        }
        return lastUsageDate;
    }

    public void updateLastUsageDate() {
        savePrefString(KEY_LAST_USAGE_DATE, DateUtil.getDateStamp());
    }

    public String getCurrentDate() {
        return DateUtil.getDateStamp();
    }

    public void ratingSubmitted() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (context != null) {
            editor.putBoolean(getKey(KEY_RATING_SUBMITTED), true);
            editor.commit();
        }
    }


    public Boolean isFirstInstallCheck() {
        return loadPrefBoolean(getKey(KEY_FIRST_INSTALL_CHECK),true);
    }

    public void setFirstInstallCheck(boolean value) {
        savePrefBoolean(getKey(KEY_FIRST_INSTALL_CHECK),value);
    }

    public Boolean isRatingNegativeButtonPressedAndNotShow() {
        return loadPrefBoolean(getKey(KEY_NEGATIVE_BUTTON_PRESSED));
    }

     public void setRatingNegativeButtonPressedAndNotShow(boolean value) {
        savePrefBoolean(getKey(KEY_NEGATIVE_BUTTON_PRESSED),value);
    }

    public Boolean isRatingShowOnlyOnce() {
        return loadPrefBoolean(getKey(KEY_RATING_SHOW_ONCE));
    }

     public void setRatingShowOnlyOnce(boolean value) {
        savePrefBoolean(getKey(KEY_RATING_SHOW_ONCE),value);
    }

    public Boolean isRatingSubmitted() {
        return loadPrefBoolean(getKey(KEY_RATING_SUBMITTED));
    }
}
