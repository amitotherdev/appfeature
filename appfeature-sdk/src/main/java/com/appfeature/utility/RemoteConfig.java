package com.appfeature.utility;

import android.app.Activity;
import android.text.TextUtils;

import com.appfeature.BuildConfig;
import com.appfeature.R;
import com.appfeature.interfaces.RemoteCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.NonNull;


public class RemoteConfig {

    private static final String KEY_APPSFEATURE = "_appsfeature";
    private final Activity activity;
    private final String packageName;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private RemoteCallback remoteCallback;

    private RemoteConfig(Activity activity) {
        this.activity = activity;
        this.packageName = activity.getApplicationContext().getPackageName();
    }

    public static RemoteConfig newInstance(Activity activity) {
        return new RemoteConfig(activity);
    }


    public RemoteConfig initialize() {
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600L)
                .build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);

        //set defaults
        mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);
        return this;
    }

    public void fetch(final RemoteCallback remoteCallback) {
        this.remoteCallback = remoteCallback;
        // cacheExpirationSeconds is set to cacheExpiration here, indicating the next fetch request
        // will use fetch data from the Remote Config service, rather than cached parameter values,
        // if cached parameter values are more than cacheExpiration seconds old.
        // See Best Practices in the README for more information.
//        long cacheExpiration = 43200;// 12 hours in seconds.
        long cacheExpiration;
        if (BuildConfig.DEBUG) {
            cacheExpiration = 0;
        } else {
            cacheExpiration = mFirebaseRemoteConfig.getInfo().getConfigSettings().getMinimumFetchIntervalInSeconds();
        }

        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String status;
                        if (task.isSuccessful()) {
                            status = Const.SUCCESS;
                            // After config data is successfully fetched, it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activate();
                        } else {
                            status = Const.FAILURE;
                        }
                        displayWelcomeMessage(status);
                    }
                });
    }

    private void displayWelcomeMessage(String status) {
        String configKey = getAppsfeatureKey();
        String ratingAndUpdate = mFirebaseRemoteConfig.getString(configKey);
        if(TextUtils.isEmpty(ratingAndUpdate)){
            if(remoteCallback !=null){
                remoteCallback.onError("Firebase config is not setup yet!");
            }
            return;
        }
        try {
            JSONObject ratingAndUpdateObj = new JSONObject(ratingAndUpdate);
            JSONObject ratingObj = ratingAndUpdateObj.getJSONObject("rating");
            JSONObject updateObj = ratingAndUpdateObj.getJSONObject("app_update");
            Rating rating = Utility.parseRatingJsonData(ratingObj);

            Version version = Utility.parseUpdateJsonData(updateObj);

            if(remoteCallback !=null){
                remoteCallback.onComplete(status, new RemoteModel(version,rating));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            if(remoteCallback !=null){
                remoteCallback.onError(e.toString());
            }
        }
    }

    private String getAppsfeatureKey() {
        return packageName.replaceAll("\\.","_")+KEY_APPSFEATURE;
    }
}
